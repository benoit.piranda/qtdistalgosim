#include "myagent.h"

MyAgent::MyAgent(unsigned int p_id,const QPointF &p_position):Agent(p_id,p_position) {}

void MyAgent::start() {
    setBlink();
    if (getId()==1) {
        setColor(0);
        setText("A");
    } else {
        setColor(9);
    }
}

void MyAgent::messageCallback(MessagePtr ptr, unsigned int senderId) {
}
