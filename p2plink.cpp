#include "p2plink.h"
#include "agent.h"
#include <QtMath>

P2PLink::P2PLink(Agent *p_A1,Agent *p_A2,qint64 p_delay):
    agent1(p_A1),agent2(p_A2),delay(p_delay) {
}

void P2PLink::draw(QPainter &painter) {
    painter.setPen(QPen(Qt::black,5));
    QPointF pos1=agent1->getPosition(),
            pos2=agent2->getPosition();
    painter.drawLine(pos1,pos2);

    painter.save();
    painter.translate(0.5*(pos1+pos2));
    QString str="["+QString::number(delay)+"]";
    QFont font("times", 12);
    QFontMetrics fm(font);
    int pixelsWide = fm.horizontalAdvance(str)+20;
    int pixelsHigh = fm.height()+10;
    QRectF rect(-pixelsWide/2,-pixelsHigh,pixelsWide,pixelsHigh);

    painter.setBrush(QBrush(Qt::yellow));
    painter.setPen(Qt::NoPen);
    painter.drawRect(rect);
    painter.setPen(QPen(Qt::black,1));
    painter.setFont(font);

    painter.drawText(rect,Qt::AlignVCenter|Qt::AlignCenter,str);
    painter.restore();

}
