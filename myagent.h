#ifndef MYAGENT_H
#define MYAGENT_H

#include "agent.h"

class MyAgent : public Agent {
public:
    MyAgent(unsigned int p_id,const QPointF &p_position);

    void start() override;
    void messageCallback(MessagePtr ptr,unsigned int senderId) override;
private :
};

#endif // MYAGENT_H
